#!/bin/bash

statusgit="$(git status --short)"
DIR="$(cd "$(dirname "$0")" && pwd)"
echo "CHECKING PHP FILES TRACKED BY git status"
for i in ${statusgit};
do
  if [[ $i == *".php"* ]]; then
    echo "CHECKING THE FILE : ./$i"
    echo "PHPLINT : Find syntax error"
    $DIR/vendor/overtrue/phplint/bin/phplint "./$i"
    echo "PHPCBF : Trying to fix php check style"
    phpcbf --standard=Symfony "./$i"
    echo "PHPCS : Check style according to symfony coding standard"
    phpcs --standard=Symfony "./$i"
    echo "PHPMD"
    phpmd "./$i" text "./config/phpmd.xml"
  fi;
done