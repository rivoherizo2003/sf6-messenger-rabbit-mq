#!/bin/bash

echo "Security checker"
#Check vulnerabilities of packages
local-php-security-checker --path=./composer.lock

echo "PHP Lint"
#Check syntax error of php files
./vendor/overtrue/phplint/bin/phplint ./src

echo "PHPCBF"
#Try to fix cs
phpcbf --standard=Symfony ./src

echo "PHPCS"
#Check style : symfony coding standard
phpcs --colors -v --standard=Symfony --ignore=./src/Kernel.php ./src

#echo "PHPMD"
##Check complexity level, code duplication,...
#phpmd ./src/ text config/phpmd.xml --exclude src/Kernel.php src/Command --suffixes php,twig
#
#echo "PHPSTAN"
##PhpStan : finding errors in your code without actually running it
#vendor/bin/phpstan analyse -l 5 ./src

echo "PHPUNIT"
php bin/phpunit