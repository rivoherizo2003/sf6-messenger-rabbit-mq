Symfony Demo Application && Messenger component && Rabbitmq
========================

The "Symfony Demo Application" is a reference application created to show how
to develop applications following the [Symfony Best Practices][1].

Requirements
------------

  * docker installed on your pc

Installations of dependencies
------------

Clone the project

```bash
$ git clone https://gitlab.com/rivoherizo2003/sf6-messenger-rabbit-mq
```

Move to the current project and launch these commands
```bash
$ docker-compose up -d
```

Install dependencies symfony
```bash
$ docker exec -i sf6_php_rbbt_mq composer install
```

Configure phpcs with symfony coding standard
```bash
$ docker exec -i sf6_php_rbbt_mq phpcs --config-set installed_paths vendor/escapestudios/symfony2-coding-standard
```

Create database
```bash
$ docker exec -i sf6_php_rbbt_mq php bin/console doctrine:database:create
```

Migrate table
```bash
$ docker exec -i sf6_php_rbbt_mq php bin/console doctrine:migrations:migrate
```

Load fixtures
```bash
$ docker exec -i sf6_php_rbbt_mq php bin/console doctrine:fixtures:load
```

Install node dependencies
```bash
$ docker exec -i sf6_node_rbbt_mq yarn install
```

Compile assets
```bash
$ docker exec -i sf6_node_rbbt_mq yarn run encore dev
```

Url to launch the symfony demo
- http://localhost:9556/

Tests
-----

Execute this command to run tests:

```bash
$ cd my_project/
$ ./bin/phpunit
```

Drop table database test
----
```bash
 docker exec -i sf6_php_rbbt_mq php bin/console d:s:d --env=test --force --full-database
```
```bash
 docker exec -i sf6_php_rbbt_mq php bin/console d:m:m --env=test
```
```bash  
 docker exec -i sf6_php_rbbt_mq php bin/console d:f:l --env=test
```

Note:
---
+ Apparently there is no PECL AMQP compatiblee with PHP 8.0, so we won't be able to use docker-php-ext-install amqp && docker-php-ext-enable amqp. Instead, with have to use dev version and build it( Source: https://exploit.cz/how-to-compile-amqp-extension-for-php-8-0-via-multistage-dockerfile/);
