<?php

namespace App\Tests\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ContactControllerTest extends WebTestCase
{
    /**
     * @dataProvider getUrls
     * @param string $httpMethod
     * @param string $url
     */
    public function testShowListContactAllowedUser(string $httpMethod, string $url): void
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'jane_admin',
            'PHP_AUTH_PW' => 'kitten'
        ]);
        $client->request($httpMethod, $url);

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    /**
     * @dataProvider getUrls
     * @param string $httpMethod
     * @param string $url
     */
    public function testShowListContactDeniedUser(string $httpMethod, string $url): void
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'john_user',
            'PHP_AUTH_PW' => 'kitten'
        ]);
        $client->request($httpMethod, $url);

        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    public function getUrls(): ?\Generator
    {
        yield ['GET', '/en/admin/post/'];
    }
}
