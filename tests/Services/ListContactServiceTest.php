<?php

namespace App\Tests\Services;

use App\Services\ListContactService;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ListContactServiceTest extends KernelTestCase
{
    public function testGetContactList()
    {
        $objectManager = $this->createMock(ObjectManager::class);
        $listContactService = new ListContactService($objectManager);

        self::assertIsArray($listContactService->getListContact());
    }
}
