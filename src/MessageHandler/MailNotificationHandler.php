<?php

/*
 * Mail notification handler
 */

namespace App\MessageHandler;

use App\Entity\Post;
use App\Handler\MailHandler;
use App\Message\MailNotification;
use App\Repository\PostRepository;
use App\Utils\Email;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Class MailNotificationHandler
 */
class MailNotificationHandler implements MessageHandlerInterface
{
    /**
     * MailNotificationHandler constructor.
     * @param MailerInterface        $mailer
     * @param PostRepository         $postRepository
     * @param MailHandler            $mailHandler
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(private MailerInterface $mailer, private PostRepository $postRepository, private MailHandler $mailHandler, private EntityManagerInterface $entityManager)
    {
    }

    /**
     * @param MailNotification $mailNotification
     *
     * @return void
     *
     * @throws Exception
     */
    public function __invoke(MailNotification $mailNotification)
    {
        $postPublished = $this->postRepository->findOneBy(['id' => $mailNotification->getId()]);
        $mail = new Email();
        $mail->setSender("sender@sf6tuto.com");
        $mail->setRecipient("user@gmail.com");
        $mail->setSubject("Article published:[".$postPublished->getTitle()."]");
        $mail->setTemplate("admin/blog/mail_content_published_article.html.twig");
        $mail->setParameters([
            'postPublished' => $postPublished,
        ]);
        try {
            sleep(10);
            $this->mailHandler->sendEmail($mail, $this->mailer);
            /** @var Post $postPublished */
            $postPublished->setIPublished(Post::POST_PUBLISHED);
            $this->entityManager->flush();
        } catch (TransportExceptionInterface $e) {
            throw new Exception($e->getMessage());
        }
    }
}
