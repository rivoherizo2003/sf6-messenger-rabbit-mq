<?php

/*
 * Class used for mail notification
 */

namespace App\Message;

/**
 * Class MailNotification
 */
class MailNotification
{
    /**
     * MailNotification constructor.
     * @param string $description
     * @param int    $id
     * @param string $from
     */
    public function __construct(private string $description, private int $id, private string $from)
    {
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom(string $from): void
    {
        $this->from = $from;
    }
}
