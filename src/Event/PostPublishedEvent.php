<?php

/*
 * Event published
 */

namespace App\Event;

/**
 * Class PostPublishedEvent
 */
class PostPublishedEvent
{
    /**
     * PostPublishedEvent constructor.
     * @param string $postTitle
     * @param int    $postId
     * @param string $from
     */
    public function __construct(protected string $postTitle, protected int $postId, protected string $from)
    {
    }

    /**
     * @return string
     */
    public function getPostTitle(): string
    {
        return $this->postTitle;
    }

    /**
     * @return int
     */
    public function getPostId(): int
    {
        return $this->postId;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }
}
