<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Event;

use App\Entity\Comment;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class CommentCreatedEvent
 */
class CommentCreatedEvent extends Event
{
    /**
     * CommentCreatedEvent constructor.
     * @param Comment $comment
     */
    public function __construct(protected Comment $comment)
    {
    }

    /**
     * @return Comment
     */
    public function getComment(): Comment
    {
        return $this->comment;
    }
}
