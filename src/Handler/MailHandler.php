<?php

/**
 * Created by PhpStorm.
 * User: ozireh
 * Date: 4/20/20
 * Time: 5:38 PM
 */

namespace App\Handler;

use App\Utils\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;

/**
 * Class MailHandler
 */
class MailHandler
{
    /**
     * @param Email           $mailToHandle
     * @param MailerInterface $mailer
     *
     * @return void
     *
     * @throws TransportExceptionInterface
     */
    public function sendEmail(Email $mailToHandle, MailerInterface $mailer)
    {
        if (!empty($mailToHandle->getSender())) {
            $email = (new TemplatedEmail())
                ->from($mailToHandle->getSender())
                ->to($mailToHandle->getRecipient())
                ->subject($mailToHandle->getSubject())
                ->htmlTemplate($mailToHandle->getTemplate())
                ->context($mailToHandle->getParameters());

            if (!is_null($mailToHandle->getCcRecipients()) && count($mailToHandle->getCcRecipients()) > 0) {
                $addresses = $mailToHandle->getCcRecipients();
                $size = count($addresses);
                for ($i = 0; $i <= $size - 1; $i++) {
                    $email->addCc($addresses[$i]);
                }
            }

            $mailer->send($email);
        }
    }
}
