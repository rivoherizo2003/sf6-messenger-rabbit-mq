<?php

/*
 * List contact service
 */

namespace App\Services;

/**
 * Class ListContactService
 */
class ListContactService
{
    use BaseServiceTrait;

    /**
     * @return array
     */
    public function getListContact(): array
    {
        return [];
    }
}
