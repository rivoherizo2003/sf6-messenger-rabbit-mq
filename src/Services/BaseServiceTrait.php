<?php
/*
 * Base service
 */
namespace App\Services;

use Doctrine\Persistence\ObjectManager;

/**
 * Trait BaseServiceTrait
 */
trait BaseServiceTrait
{
    /**
     * @var ObjectManager
     */
    protected ObjectManager $objectManager;

    /**
     * BaseServiceTrait constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
    }
}
