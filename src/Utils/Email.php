<?php

/*
 * Class util Email
 */

/**
 * Created by PhpStorm.
 * User: user
 * Date: 4/20/20
 * Time: 5:42 PM
 */

namespace App\Utils;

/**
 * Class Email
 */
class Email
{
    protected string $sender;
    protected string $recipient;
    protected array $ccRecipients = [];
    protected string $subject;
    protected array $parameters = [];
    protected string $template;
    protected string $content;

    /**
     * @return string
     */
    public function getSender(): string
    {
        return $this->sender;
    }

    /**
     * @param string $sender
     */
    public function setSender(string $sender): void
    {
        $this->sender = $sender;
    }

    /**
     * @return string
     */
    public function getRecipient(): string
    {
        return $this->recipient;
    }

    /**
     * @param string $recipient
     */
    public function setRecipient(string $recipient): void
    {
        $this->recipient = $recipient;
    }

    /**
     * @return array
     */
    public function getCcRecipients(): array
    {
        return $this->ccRecipients;
    }

    /**
     * @param array $ccRecipients
     */
    public function setCcRecipients(array $ccRecipients): void
    {
        $this->ccRecipients = $ccRecipients;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     */
    public function setParameters(array $parameters): void
    {
        $this->parameters = $parameters;
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * @param string $template
     */
    public function setTemplate(string $template): void
    {
        $this->template = $template;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }
}
