<?php

/*
 * Post subscriber
 */

namespace App\EventSubscriber;

use App\Event\PostPublishedEvent;
use App\Message\MailNotification;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class PostSubscriber
 */
class PostSubscriber implements EventSubscriberInterface
{
    /**
     * PostSubscriber constructor.
     * @param MessageBusInterface $messageBus
     */
    public function __construct(private MessageBusInterface $messageBus)
    {
    }

    /**
     * @return string[]
     */
    #[ArrayShape([PostPublishedEvent::class => "string"])]
    public static function getSubscribedEvents(): array
    {
        return [
            PostPublishedEvent::class => 'onPostPublished',
        ];
    }

    /**
     * @param PostPublishedEvent $postPublishedEvent
     */
    public function onPostPublished(PostPublishedEvent $postPublishedEvent)
    {
        $this->messageBus->dispatch(new MailNotification(
            $postPublishedEvent->getPostTitle(),
            $postPublishedEvent->getPostId(),
            $postPublishedEvent->getFrom()
        ));
    }
}
