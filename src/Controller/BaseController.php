<?php

/*
 * all controllers should extends this class
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class BaseController
 */
class BaseController extends AbstractController
{
}
