<?php

/*
 *
 * This class was created by ozireh Ma
 *
 */

namespace App\Controller\Admin;

use App\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContactController
 */
#[Route('/admin/contact'), isGranted('ROLE_ADMIN')]
class ContactController extends BaseController
{
    /**
     * @return Response
     */
    #[
        Route('/', name: 'contact_list', methods: ['GET'])
    ]
    public function showListContact(): Response
    {
        return $this->render('admin/contact/index.html.twig');
    }
}
